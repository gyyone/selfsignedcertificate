﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenSSL.Core;
using OpenSSL.Crypto;
using OpenSSL.X509;

namespace CertificateGenerator
{
    public static class Helper
    {
        public static CryptoKey CreateNewRsaKey(int numberOfBits)
        {
            var rsa = new RSA();
                BigNumber exponent = 0x10001; // this needs to be a prime number
                rsa.GenerateKeys(numberOfBits, exponent, null, null);

                return new CryptoKey(rsa);
            
        }
        public static void WriteCert(X509Certificate cert, string filepath)
        {
            using (var biocert = BIO.File(filepath, "w"))
            {
                cert.Write(biocert);
            }
        }
        public static void WriteCert(X509CertificateAuthority cert,string filepath)
        {
            using (var biocert = BIO.File(filepath, "w"))
            {
                cert.Certificate.Write(biocert);
            }           
        }

        public static void WriteKey(CryptoKey key,string password,string filepath)
        {
            using (var bioprivate = BIO.File(filepath, "w"))
            {
                key.WritePrivateKey(bioprivate, Cipher.DES_EDE3_CBC, password);
            }
               
        }
    }
}

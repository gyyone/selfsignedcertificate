﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

using OpenSSL.Core;
using OpenSSL.Crypto;
using OpenSSL.X509;
using System.Dynamic;


namespace CertificateGenerator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ISequenceNumber sn = new SimpleSerialNumber();
      
            CryptoKey key = Helper.CreateNewRsaKey(4096);

        
        X509Name subject = new X509Name();
            subject.Common = "Gyyone1";
            subject.Country = "MY";
            subject.Organization = "Gyyone";
            subject.StateOrProvince = "Penang";
            subject.OrganizationUnit = "Gyyone";

            var extensions = new List<OpenSSL.X509.X509V3ExtensionValue>();
            extensions.Add(new OpenSSL.X509.X509V3ExtensionValue("basicConstraints", true, "CA:TRUE"));
            extensions.Add(new OpenSSL.X509.X509V3ExtensionValue("subjectKeyIdentifier", false, "hash"));
            extensions.Add(new OpenSSL.X509.X509V3ExtensionValue("authorityKeyIdentifier", false, "keyid:always,issuer:always"));

            X509CertificateAuthority CA = X509CertificateAuthority.SelfSigned(sn,key, MessageDigest.SHA512, subject, DateTime.Now, TimeSpan.FromDays(560), extensions);
            // Write the CA certificate and it's private key to file so that it can be re-used.
            Helper.WriteCert(CA,"CA.cer");
            Helper.WriteKey(key,"123456", "CAPrivate.key");
        

         //Create Signed Request
            CryptoKey key2 = Helper.CreateNewRsaKey(4096);
            X509Name subject2 = new X509Name();
            subject.Common = "Gyyone2";
            subject.Country = "MY";
            subject.Organization = "Gyyone";
            subject.StateOrProvince = "Penang";
            subject.OrganizationUnit = "Gyyone";
         
            X509Request request = new X509Request(2, subject2, key2);
            string path = Path.GetFullPath("openssl.cnf");
            OpenSSL.X509.Configuration temp = new Configuration("X64\\openssl.cnf");

           var signedcert = CA.ProcessRequest(request, DateTime.UtcNow, DateTime.UtcNow.AddYears(1), temp, "v3_ca", MessageDigest.SHA512);

            Helper.WriteCert(signedcert, "CA2.cer");
            Helper.WriteKey(key2, "123456", "CAPrivate2.key");

        }
    }
}
